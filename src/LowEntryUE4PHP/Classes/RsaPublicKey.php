<?php

namespace LowEntryUE4PHP\Classes;

use LowEntryUE4PHP\Classes\Internal\Rsa\RsaBigInteger;


class RsaPublicKey
{
	/** @var RsaBigInteger */
	public $n;
	/** @var RsaBigInteger */
	public $e;
	
	
	/**
	 * @param RsaBigInteger $n
	 * @param RsaBigInteger $e
	 */
	public function __construct($n, $e)
	{
		$this->n = $n;
		$this->e = $e;
	}
}
