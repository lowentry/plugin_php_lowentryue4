<?php

namespace LowEntryUE4PHP\Classes;


class AesKey1D
{
	/** @var int */
	public $rounds;
	/** @var int[] Integer array */
	public $encryptionW;
	/** @var int[] Integer array */
	public $decryptionW;
	
	
	/**
	 * @param int   $rounds
	 * @param int[] $encryptionW Integer array
	 * @param int[] $decryptionW Integer array
	 */
	public function __construct($rounds, $encryptionW, $decryptionW)
	{
		$this->rounds = $rounds;
		$this->encryptionW = $encryptionW;
		$this->decryptionW = $decryptionW;
	}
}
