<?php

namespace LowEntryUE4PHP\Classes;


class RsaKeys
{
	/** @var RsaPublicKey */
	public $publicKey;
	/** @var RsaPrivateKey */
	public $privateKey;
	
	
	/**
	 * @param RsaPublicKey  $publicKey
	 * @param RsaPrivateKey $privateKey
	 */
	public function __construct($publicKey, $privateKey)
	{
		$this->publicKey = $publicKey;
		$this->privateKey = $privateKey;
	}
}
