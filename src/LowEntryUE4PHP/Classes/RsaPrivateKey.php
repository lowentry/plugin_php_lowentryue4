<?php

namespace LowEntryUE4PHP\Classes;

use LowEntryUE4PHP\Classes\Internal\Rsa\RsaBigInteger;
use LowEntryUE4PHP\Classes\Internal\Rsa\RsaBigIntegerHelper;


class RsaPrivateKey
{
	/** @var RsaBigInteger */
	public $n;
	/** @var RsaBigInteger */
	public $d;
	
	/** @var RsaBigInteger */
	public $p;
	/** @var RsaBigInteger */
	public $q;
	/** @var RsaBigInteger */
	public $dp;
	/** @var RsaBigInteger */
	public $dq;
	/** @var RsaBigInteger */
	public $c2;
	
	
	/**
	 * @param RsaBigInteger $n
	 * @param RsaBigInteger $d
	 * @param RsaBigInteger $p
	 * @param RsaBigInteger $q
	 */
	public function __construct($n, $d, $p, $q)
	{
		$this->n = $n;
		$this->d = $d;
		$this->p = $p;
		$this->q = $q;
		
		if(($p !== null) && ($q !== null))
		{
			$pm1 = $p->subtract(RsaBigIntegerHelper::ONE());
			if($pm1 !== null)
			{
				$qm1 = $q->subtract(RsaBigIntegerHelper::ONE());
				if($qm1 !== null)
				{
					$dp = RsaBigIntegerHelper::remainder($d, $pm1);
					if($dp !== null)
					{
						$dq = RsaBigIntegerHelper::remainder($d, $qm1);
						if($dq !== null)
						{
							$c2 = $p->modInverse($q);
							if($c2 !== null)
							{
								$this->dp = $dp;
								$this->dq = $dq;
								$this->c2 = $c2;
								return;
							}
						}
					}
				}
			}
		}
		
		$this->dp = null;
		$this->dq = null;
		$this->c2 = null;
	}
}
