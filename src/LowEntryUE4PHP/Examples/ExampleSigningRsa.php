<?php

namespace LowEntryUE4PHP\Examples;

use LowEntryUE4PHP\LowEntry;


class ExampleSigningRsa
{
	public static function run()
	{
		$keysA = LowEntry::generateRsaKeys(1024);
		
		if($keysA === null)
		{
			LowEntry::debug('keys were null');
			return;
		}
		
		
		// both sides have access to the document's data
		$documentBytes = LowEntry::stringToBytesUtf8("the document's data");
		
		// A signs the document
		$signature = LowEntry::signRsa(LowEntry::hash('sha256', $documentBytes), $keysA->privateKey);
		
		// A sends B the public key (along with the signature)
		$publicKeyBytesA = LowEntry::rsaPublicKeyToBytes($keysA->publicKey);
		
		// B receives the public key (along with the signature)
		$publicKeyA = LowEntry::bytesToRsaPublicKey($publicKeyBytesA);
		
		// B verifies the signature
		LowEntry::debug(LowEntry::verifySignatureRsa($signature, LowEntry::hash('sha256', $documentBytes), $publicKeyA));
	}
}
