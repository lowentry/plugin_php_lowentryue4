<?php

namespace LowEntryUE4PHP\Examples;

use LowEntryUE4PHP\LowEntry;


class ExampleCompression
{
	public static function run()
	{
		$bytes = LowEntry::stringToBytesUtf8('test aaaaaaaadsfdsfgaaaaaaaa test aaaaaaaaaaaaaaedsfdsfgaaaaasdgsdgaaaaaa');
		LowEntry::debug(LowEntry::bytesToHex($bytes));
		
		$compressed = LowEntry::compressLzf($bytes);
		LowEntry::debug(LowEntry::bytesToHex($compressed));
		
		$decompressed = LowEntry::decompressLzf($compressed);
		LowEntry::debug(LowEntry::bytesToHex($decompressed));
		
		$string = LowEntry::bytesToStringUtf8($decompressed);
		LowEntry::debug($string);
	}
}
