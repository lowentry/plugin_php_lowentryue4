<?php

namespace LowEntryUE4PHP\Examples;

use LowEntryUE4PHP\LowEntry;


class ExampleEncryptionRsa
{
	public static function run()
	{
		$keys = LowEntry::generateRsaKeys(1024);
		
		if($keys === null)
		{
			LowEntry::debug('keys were null');
			return;
		}
		
		$data = LowEntry::stringToBytesUtf8('very top secret data');
		LowEntry::debug(LowEntry::bytesToHex($data));
		
		$encrypted = LowEntry::encryptRsa($data, $keys->publicKey);
		LowEntry::debug(LowEntry::bytesToHex(LowEntry::rsaPrivateKeyToBytes($keys->privateKey)));
		LowEntry::debug(LowEntry::bytesToHex($encrypted));
		
		$decrypted = LowEntry::decryptRsa($encrypted, $keys->privateKey);
		LowEntry::debug(LowEntry::bytesToHex($decrypted));
		
		$string = LowEntry::bytesToStringUtf8($decrypted);
		LowEntry::debug($string);
	}
}
