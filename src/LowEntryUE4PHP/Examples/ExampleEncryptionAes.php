<?php

namespace LowEntryUE4PHP\Examples;

use LowEntryUE4PHP\LowEntry;


class ExampleEncryptionAes
{
	public static function run()
	{
		$key = LowEntry::stringToBytesUtf8('key');
		LowEntry::debug(LowEntry::bytesToHex($key));
		
		$data = LowEntry::stringToBytesUtf8('very top secret data');
		LowEntry::debug(LowEntry::bytesToHex($data));
		
		$encrypted = LowEntry::encryptAes($data, $key);
		LowEntry::debug(LowEntry::bytesToHex($encrypted));
		
		$decrypted = LowEntry::decryptAes($encrypted, $key);
		LowEntry::debug(LowEntry::bytesToHex($decrypted));
		
		$string = LowEntry::bytesToStringUtf8($decrypted);
		LowEntry::debug($string);
	}
}
